package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.Mutation;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccountDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface TDepositAccountDetailRepo extends PagingAndSortingRepository<TDepositAccountDetail,Long> {

    @Query(value="SELECT * FROM t_deposit_account_detail tdad "
            + "WHERE tdad.id = :id " + "AND tdad.is_deleted = false", nativeQuery= true)
    TDepositAccountDetail findByIdTrue(@Param("id") Long id);

    Iterable<TDepositAccountDetail> findByMutationStatus (Mutation mutation);

    @Query(value = "Select * from t_deposit_account_detail tdad order by tdad.id desc limit 1",nativeQuery = true)
    Optional<TDepositAccountDetail> findRefByRefOrderByIdDescLimit1();

    @Query(value = " Select * from m_transaction_code mtc "
            + "WHERE mta.transaction_code_id = :mtcId ", nativeQuery = true)
    TDepositAccount findByCode(@Param("mtcId")Long mtransactionCode);




}
