package com.b2camp.teamc.transactionservice.teamc.controllers;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TDepositAccountDetailRequest;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccountDetail;
import com.b2camp.teamc.transactionservice.teamc.services.TDepositAccountDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(name = "/depositaccountdetail")
public class TDepositAccountDetailController {

    @Autowired
    private TDepositAccountDetailService tDepositAccountDetailService;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public TDepositAccountDetail add(@RequestBody TDepositAccountDetailRequest request){
        return tDepositAccountDetailService.createTDepositAccountDetail(request);//.add(request);
    }


    @DeleteMapping("/delete/{id}")
    public Map<String, Object> delete(@RequestParam("id") Long id) {
        return tDepositAccountDetailService.softDeleteTDepositAccountDetail(id);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Object> update(@RequestParam Long id, @RequestBody TDepositAccountDetailRequest tDepositAccountDetailRequest) {
        return new ResponseEntity<>(tDepositAccountDetailService.updateSaldo(id, tDepositAccountDetailRequest),HttpStatus.OK);
    }

    @GetMapping
    public Iterable<TDepositAccountDetail> getJoinInformation() {
        return tDepositAccountDetailService.findAllDataTDepositAccountDetail();
    }

}
