package com.b2camp.teamc.transactionservice.teamc.models;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import com.b2camp.teamc.transactionservice.teamc.models.references.MTransactionCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name="t_journal_ledger")
@Table
@SuperBuilder
public class TJournalLedger extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id",unique = true, nullable = false,length = 50)
    private Long id;

    @Column(name = "description", nullable = false,length = 100)
    private String description;

    @ManyToOne(targetEntity = TSavingAccountDetail.class,cascade=CascadeType.ALL)
    @JoinColumn(name = "t_saving_account_detail_id", referencedColumnName = "id")
    private TSavingAccountDetail tSavingAccountDetail;

    @ManyToOne(targetEntity = MTransactionCode.class,cascade=CascadeType.ALL)
    @JoinColumn(name = "m_transaction_code_id", referencedColumnName = "id")
    private MTransactionCode mTransactionCode;

    @Column(name = "nominal", nullable = false,length = 100)
    private BigDecimal nominal;

}
