package com.b2camp.teamc.transactionservice.teamc.models.references;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="r_status")
@SuperBuilder
public class RStatus extends BaseEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id",unique = true, nullable = false,length = 50)
        private Long id;
        @NotEmpty(message = "Code is mandatory.")
        @Column(name = "code",nullable = false,unique = true)
        private String code;
        @NotEmpty(message="Name is mandatory.")
        @Column(name = "name",nullable = false,unique = true)
        private String name;
}
