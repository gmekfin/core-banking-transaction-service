package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.references.RStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RStatusRepo extends JpaRepository<RStatus,Long> {
}
