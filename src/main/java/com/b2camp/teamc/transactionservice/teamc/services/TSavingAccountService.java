package com.b2camp.teamc.transactionservice.teamc.services;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TSavingAccountRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TSavingAccountResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;

import java.util.List;
import java.util.Map;

public interface TSavingAccountService {
    TSavingAccountResponse add(TSavingAccountRequest request);
    Map<String, Object> delete(Long id);
    List<TSavingAccount> getJoinInformation();
    TSavingAccountResponse getById(Long id);
}
