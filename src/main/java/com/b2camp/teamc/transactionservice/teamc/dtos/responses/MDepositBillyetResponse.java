package com.b2camp.teamc.transactionservice.teamc.dtos.responses;

import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class MDepositBillyetResponse {
    private Long id;
    private String bilYetNumber;
    private TDepositAccount tDepositAccount;
}
