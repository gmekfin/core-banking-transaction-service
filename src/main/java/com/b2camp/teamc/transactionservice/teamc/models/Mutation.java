package com.b2camp.teamc.transactionservice.teamc.models;

public enum Mutation {
    DEBIT,
    CREDIT;

}
