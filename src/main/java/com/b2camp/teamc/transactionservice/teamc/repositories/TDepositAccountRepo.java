package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface TDepositAccountRepo extends PagingAndSortingRepository<TDepositAccount,Long> {
    @Query(value="SELECT * FROM t_deposit_account tda "
            + "WHERE tda.id = :id " + "AND tda.is_deleted = false", nativeQuery= true)
    TDepositAccount findByIdTrue(@Param("id") Long id);
    Optional<TDepositAccount> findByAccountNumber(String accountNumber);


    @Query(value = "Select * from t_deposit_account tda "
    +"WHERE tda.due_Date is null",nativeQuery = true)
    List<TDepositAccount> findDateIsNull(Pageable pageable);


}
