package com.b2camp.teamc.transactionservice.teamc.models;

import com.b2camp.teamc.transactionservice.teamc.models.baseentity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="m_deposit_bilyet")
@SuperBuilder
public class MDepositBillyet extends BaseEntity {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id",unique = true, nullable = false,length = 50)
        private Long id;

        @Column(name = "bilyet_number",nullable = false,length = 50)
        private String bilYetNumber;

        @OneToOne(cascade = CascadeType.ALL, targetEntity = TDepositAccount.class)
        @JoinColumn(name = "t_deposit_account_id",referencedColumnName = "id")
        private TDepositAccount tDepositAccount;
}
