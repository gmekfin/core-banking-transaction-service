package com.b2camp.teamc.transactionservice.teamc.dtos.requests;

import com.b2camp.teamc.transactionservice.teamc.models.Mutation;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@Builder
@ToString
public class TDepositAccountDetailRequest {
    private String tDepositAccount;

    @Builder.Default private Mutation mutationType = Mutation.CREDIT;
    private BigDecimal transactionAmmount;
    private String transactionCode;
    private String tSavingAccount;



}
