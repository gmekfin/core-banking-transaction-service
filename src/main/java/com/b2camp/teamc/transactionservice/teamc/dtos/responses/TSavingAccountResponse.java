package com.b2camp.teamc.transactionservice.teamc.dtos.responses;

import com.b2camp.teamc.transactionservice.teamc.models.references.MCif;
import com.b2camp.teamc.transactionservice.teamc.models.references.MSavingProduct;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
@Data
@Builder
@AllArgsConstructor
public class TSavingAccountResponse {

        private Long id;
        private MCif mCifId;
        private MSavingProduct mSavingProductId;
        private String accountNumber;
        private BigDecimal balance;
}
