package com.b2camp.teamc.transactionservice.teamc.dtos.responses;

import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccount;
import com.b2camp.teamc.transactionservice.teamc.models.TSavingAccount;
import com.b2camp.teamc.transactionservice.teamc.models.references.MCif;
import com.b2camp.teamc.transactionservice.teamc.models.references.MDepositProduct;
import com.b2camp.teamc.transactionservice.teamc.models.references.RStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
public class TDepositAccountResponse {

    private Long id;
    private BigDecimal nominal;
    private Integer timePeriod;
    private Date startDate;
    private Date dueDate;
    private MCif mCif;
    private String accountNumber;
    private RStatus rStatus;
    private MDepositProduct productId;
    private TSavingAccount savingAccountId;
    private TDepositAccount tDepositAccount;

}
