package com.b2camp.teamc.transactionservice.teamc.services;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TDepositAccountDetailRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.TDepositAccountDetailResponse;
import com.b2camp.teamc.transactionservice.teamc.models.TDepositAccountDetail;

import java.math.BigDecimal;
import java.util.Map;

public interface TDepositAccountDetailService {
    TDepositAccountDetail createTDepositAccountDetail (TDepositAccountDetailRequest tDepositAccountDetailRequest);
    TDepositAccountDetailResponse updateSaldo (Long id,TDepositAccountDetailRequest tDepositAccountDetailRequest);
    Iterable<TDepositAccountDetail> findAllDataTDepositAccountDetail();
    Map<String, Object> softDeleteTDepositAccountDetail(Long id);
    TDepositAccountDetailResponse add(TDepositAccountDetailRequest tDepositAccountDetailRequest);
    TDepositAccountDetail creditAmmount (TDepositAccountDetailRequest tDepositAccountDetailRequest, BigDecimal creditAmmount);
    TDepositAccountDetail debitAmmount (TDepositAccountDetailRequest tDepositAccountDetailRequest,BigDecimal debitAmmount);
}
