package com.b2camp.teamc.transactionservice.teamc.repositories;

import com.b2camp.teamc.transactionservice.teamc.models.references.MCif;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MCifRepo extends JpaRepository<MCif, Long> {
}
