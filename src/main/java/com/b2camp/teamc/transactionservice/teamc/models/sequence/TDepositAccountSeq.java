package com.b2camp.teamc.transactionservice.teamc.models.sequence;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;

@Embeddable
@Entity
@Data
public class TDepositAccountSeq {

    @Id
    @GenericGenerator(name = "account_number", strategy = "com.b2camp.teamc.transactionservice.teamc.models.sequence.AccountNumberGenerator",
            parameters = {
                    @Parameter(name = "initial_value", value = "1"),
                    @Parameter(name = "increment_size", value = "1"),
            })
    @GeneratedValue(generator = "account_number_code", strategy = GenerationType.SEQUENCE)
    private String accountNumber;
}
