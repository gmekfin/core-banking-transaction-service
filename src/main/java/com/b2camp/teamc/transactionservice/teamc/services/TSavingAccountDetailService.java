package com.b2camp.teamc.transactionservice.teamc.services;

import com.b2camp.teamc.transactionservice.teamc.dtos.requests.TransactionRequest;
import com.b2camp.teamc.transactionservice.teamc.dtos.responses.SavingTransactionResponse;

import java.util.List;

public interface TSavingAccountDetailService {
    Object transaction(TransactionRequest request);
    List<SavingTransactionResponse> report();
    SavingTransactionResponse reportByRef(String ref);
}
